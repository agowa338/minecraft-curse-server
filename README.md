Builds a minecraft server by specifying the Curse Modpack Download URL. 
By using this, you also accept the Minecraft EULA. 
The source code within this repository is subject to the MIT license.
