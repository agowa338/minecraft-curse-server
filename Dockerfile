FROM openjdk:8-jre-alpine
LABEL maintainer "agowa338"
ARG ModpackURL="https://media.forgecdn.net/files/2536/782/Modern+Skyblock+3.1.5.3+Server+File.zip"
ARG AcceptMinecraftEULA="true"
ARG JarName="forge-1.12.2-14.23.2.2611-universal.jar"
ENV javaParamXms "1024M"
ENV javaParamXmx "2048M"
RUN apk add -U \
        openssl \
        imagemagick \
        lsof \
        su-exec \
        bash \
        curl \
        iputils \
        libarchive-tools \
        jq \
        python \
        python-dev \
        py2-pip
RUN rm -rf /var/cache/apk/*
RUN pip install --upgrade pip
RUN pip install mcstatus
HEALTHCHECK CMD mcstatus localhost ping
RUN addgroup -g 1000 minecraft \
  && adduser -Ss /bin/false -u 1000 -G minecraft -h /mc minecraft \
  && mkdir -p /mc \
  && mkdir -p /mc/data \
  && mkdir -p /mc/mods \
  && mkdir -p /mc/config \
  && mkdir -p /mc/plugins \
  && mkdir -p /mc/world \
  && mkdir -p /mc/backups
RUN curl -L ${ModpackURL} | bsdtar -x -C "/mc" -f -
RUN echo "eula=${AcceptMinecraftEULA}" > /mc/eula.txt
RUN ln -s "/mc/${JarName}" "/mc/start-server.jar"
RUN chown -R minecraft:minecraft /mc
EXPOSE 25565
VOLUME [ "/mc/data", "/mc/mods", "/mc/config", "/mc/plugins", "/mc/world", "/mc/backups" ]
WORKDIR /mc
ENTRYPOINT "/usr/bin/java" "-Xms"${javaParamXms} "-Xmx"${javaParamXmx} "-jar" "start-server.jar" "nogui"

